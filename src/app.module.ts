import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { GameModule } from './game/game.module';
import { WelcomeModule } from './welcome/app.module';


@Module({
  imports: [
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
      resolverValidationOptions: {
        requireResolversForResolveType: false
      },
      debug: true,
      playground: true,
      installSubscriptionHandlers: true,
      subscriptions: {
        keepAlive: 5000,
      }
    }),
    WelcomeModule,
    GameModule
  ]
})
export class AppModule { }
