import { Test, TestingModule } from '@nestjs/testing';
import { GameResolver } from './game.resolver';
import { GameService } from './game.service';

jest.mock('uuid', () => ({ v4: () => '00000000-0000-0000-0000-000000000000' }));

describe('GameResolver', () => {
    let gameResolver: GameResolver;
    let gameService: GameService;
    let gameData = []

    beforeEach(async () => {
        const app: TestingModule = await Test.createTestingModule({
            providers: [GameResolver, GameService],
        }).compile();

        gameResolver = app.get<GameResolver>(GameResolver);
        gameService = app.get<GameService>(GameService);
    });

    it('should return gameDB if query games', () => {
        expect(gameResolver.getGames()).toStrictEqual(gameData);
    });

    it('should return new Date() if mutation resetGame', () => {
        const currentDate = new Date()
        expect(gameResolver.resetGame()).toStrictEqual(currentDate);
    });

    it('should return gameDB if mutation createGame', () => {
        const currentDate = new Date()
        const type = 'BLUE'
        const returnData =
        {
            id: '00000000-0000-0000-0000-000000000000',
            type: type,
            timestamp: currentDate
        }
        expect(gameResolver.createGame(type)).toStrictEqual(returnData);
    });

    it('should throw error if shouldContinueGame false during mutation createGame', () => {
        const type = 'BLUE'
        jest.spyOn(gameService, 'shouldContinueGame').mockImplementation(() => false);
        try {
            gameResolver.createGame(type);
        } catch (error) {
            expect(error).toBeInstanceOf(Error);
            expect(error).toHaveProperty('message', 'Unable to save the record');
        }
    });
});
