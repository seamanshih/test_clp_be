import { Injectable } from '@nestjs/common';

@Injectable()
export class GameService {
    shouldContinueGame(data: any, inputDate: Date): boolean {
        const period = 5000
        const startTime = Math.min(...data.map(o => o.timestamp.getTime()), new Date().getTime());
        const endTime = inputDate.getTime()
        if (endTime - startTime <= period) {
            return true
        }
        return false
    }
}
