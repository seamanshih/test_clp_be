import { v4 as uuidv4 } from 'uuid';
import { Resolver, Query, Mutation, Args, Subscription } from '@nestjs/graphql';
import { PubSub } from 'graphql-subscriptions';
import { GameService } from './game.service';

@Resolver()
export class GameResolver {
    private pubSub: PubSub;

    constructor(private readonly gameService: GameService) {
        this.pubSub = new PubSub();
    }

    gameDB = []

    @Query('games')
    getGames() {
        return this.gameDB
    }

    @Mutation()
    resetGame() {
        this.gameDB = []
        return new Date()
    }

    @Mutation()
    createGame(@Args('type') type: string) {
        const currentDate = new Date()
        if (this.gameService.shouldContinueGame(this.gameDB, currentDate)) {
            const uuid = uuidv4()
            const newType = { id: uuid, type: type, timestamp: currentDate }
            this.gameDB.push(newType)
            this.pubSub.publish('gameAdded', { gameAdded: this.gameDB });
            return newType
        }
        throw new Error('Unable to save the record');
    }

    @Subscription()
    gameAdded() {
        return this.pubSub.asyncIterator('gameAdded');
    }

}
