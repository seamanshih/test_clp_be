import { Test, TestingModule } from '@nestjs/testing';
import { GameService } from './game.service';

describe('GameService', () => {
    let gameService: GameService;
    let gameData = []

    beforeEach(async () => {
        const app: TestingModule = await Test.createTestingModule({
            providers: [GameService],
        }).compile();

        gameService = app.get<GameService>(GameService);
    });

    describe('shouldContinueGame', () => {
        it('should return true if game Data is empty', () => {
            const currentDate = new Date('2020-08-12T11:33:46.620Z')
            expect(gameService.shouldContinueGame(gameData, currentDate)).toBe(true);
        });

        it('should return true if game Data within 5 second', () => {
            gameData = [{
                id: 'uuid-1',
                type: 'BLUE',
                timestamp: new Date('2020-08-12T11:33:44.620Z')
            }]
            const currentDate = new Date('2020-08-12T11:33:46.620Z')
            expect(gameService.shouldContinueGame(gameData, currentDate)).toBe(true);
        });

        it('should return false if game Data greater than 5 second', () => {
            gameData = [{
                id: 'uuid-1',
                type: 'BLUE',
                timestamp: new Date('2020-08-12T11:33:40.620Z')
            }]
            const currentDate = new Date('2020-08-12T11:33:46.620Z')
            expect(gameService.shouldContinueGame(gameData, currentDate)).toBe(false);
        });
    });
});
