import { Module } from '@nestjs/common';
import { GameResolver } from './game.resolver';
import { GameService } from './game.service';
import { DateScalar } from './date.scalar';

@Module({
    providers: [GameResolver, GameService, DateScalar],
    exports: [GameResolver]
})
export class GameModule { }
